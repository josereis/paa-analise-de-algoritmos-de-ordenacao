package geradorVetor;

import java.util.Random;

public class GerarVetor {
	
	public static int [] vetorCrescente(int tamanho) {
		int aux = (new Random()).nextInt(tamanho*100);
		
		int [] vetor = new int [tamanho];
		for(int i = 0; i < tamanho; i++) {
			vetor[i] = aux++;
		}
		
		return vetor;
	}
	
	public static int [] vetorDecrescente(int tamanho) {
		int aux = (new Random()).nextInt(tamanho*100);
		int [] vetor = new int [tamanho];
		for(int i = tamanho - 1; i >= 0; i--) {
			vetor[i] = aux++;
		}
		
		return vetor;
	}
	
	public static int [] vetorAleatorio(int tamanho) {
		Random r = new Random();
		
		int valorMax = tamanho*50;
		int [] vetor = new int [tamanho];
		for(int i = 0; i < tamanho; i++) {
			vetor[i] = r.nextInt(valorMax) + 1;
		}
		
		return vetor;
	}
	
}
