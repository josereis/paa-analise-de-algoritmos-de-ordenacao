package algoritmos.recursivos;

public class QuickSort {
private static int numeroComparacoes = 0;
	
	private static void troca (int i, int j, int [] vetor) {
		int aux = vetor[i];
		vetor[i] = vetor[j];
		vetor[j] = aux;
	}
	
	private static void quicksort(int ini, int fin, int [] vetor) {
		int i = ini, j = fin, pivor = vetor[ini+(fin-ini)/2];
		
		
		// DIVIDINDO EM DUAS LISTAS
		while(i <= j){
			
			while(vetor[i] < pivor){
				i++;
			}
			
			while(vetor[j] > pivor){
				j--;
			}
			
			// SE FOR ENCONTRADO NA LISTA A ESQUERDA UM ELEMENTO MAIOR QUE O PIVOR E SE TAMBEM FOR 
			// ENCONTRADO NA LISTA A DIREITA UM ELEMENTO MENOR QUE O PIVOR ENT�O TROCA-SE ESSES VALORES
			if(i <= j) {
				troca(i, j, vetor);
				i++; j--;
			}
			
		}
		
		// CHAMADA RECURSIVA
		if(ini < j) {
			quicksort(ini, j, vetor);
		}
		
		if(i < fin) {
			quicksort(i, fin, vetor);
		}
		
	}
	
	public static int quickSort(int [] vetor) {
		if(vetor.length > 1) {
			quicksort(0, vetor.length-1, vetor);
		}
		
		return numeroComparacoes;
	}
	
}
