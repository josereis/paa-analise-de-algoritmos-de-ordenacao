package algoritmos.recursivos;

public class MergeSort {
	private static int numeroComparacoes = 0;
	
	private static int [] subVetor(int ini, int fin, int [] vetor){
		int [] novo = new int [fin - ini];
		
		for(int i = ini; i < fin; i++){
			novo[i-ini] = vetor[i];
		}
		
		return novo;
	}
	
	private static void merge(int [] subVetor1, int [] subVetor2, int vetor[]){
		int SV1 = subVetor1.length, SV2 = subVetor2.length, i1 = 0, i2 = 0;
		
		for(int i = 0; i < vetor.length; i++){
			
			if(i2 == SV2 || (i1 < SV1 && subVetor1[i1] < subVetor2[i2])) {
				vetor[i] = subVetor1[i1];
				i1++;
			} else {
				vetor[i] = subVetor2[i2];
				i2++;
			}
			
			numeroComparacoes++;// POIS SEMPRE EXECULTAR� UMA COMPARA��O AQUI
		}
		
	}
	
	public static int mergeSort(int [] vetor){
		
		if(vetor.length > 1) {
			int meio = (vetor.length)/2;
			
			// CRIANDO SUBVETORES PARA DIVIS�O DO PROBLEMA
			int [] subVetor1 = subVetor(0, meio, vetor), subVetor2 = subVetor(meio, vetor.length, vetor);
			
			// CHAMADA RECURSIVA
			mergeSort(subVetor1);
			mergeSort(subVetor2);
			
			// REALIZA O MERGE ENTRE OS DOIS SUBVETORES (ETAPA DA CONQUISTA)
			merge(subVetor1, subVetor2, vetor);
		}
		
		return numeroComparacoes;
	}
	
}
