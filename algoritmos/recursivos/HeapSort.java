package algoritmos.recursivos;

public class HeapSort {
	private static int total, numeroComparacoes = 0;
	
	private static void troca(int a, int b, int [] vetor){
		int aux = vetor[a];
		vetor[a] = vetor[b];
		vetor[b] = aux;
	}
	
	private static void heap(int i, int [] vetor){
		int aux = i, esquerda = i*2, direita = (i*2 + 1);
		
		// VERIFICA SE O FILHO A ESQUERDA � MAIOR QUE O PAI
		if((esquerda < total)&&(vetor[aux] < vetor[esquerda])){
			aux = esquerda;
		}
		// VERIFICA SE O FILHO A DIREITA � MAIOR QUE O PAI
		if((direita < total)&&(vetor[aux] < vetor[direita])){
			aux = direita;
		}
		
		// SE O FILHO FOR MAIOR QUE O PAI
		if(i != aux) {
			troca(i, aux, vetor);
			heap(aux, vetor); // REFAZ O HEAP
		}
	}
	
	public static int heapSort(int [] vetor){
		total = vetor.length;
		
		// MONTA O PRIMEIRO HEAP
		for(int i = (total/2); i>= 0; i--){
			heap(i, vetor);
		}
		
		for(int i = total - 1; i > 0; i--){
			troca(0, i, vetor); // COLOCA A RAIZ DO HEAP (MAIOR ELEMENTO) NA POSI��O CORRETA
			total--;
			heap(0, vetor);
		}
		
		return numeroComparacoes;
	}
}
