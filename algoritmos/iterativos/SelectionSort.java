package algoritmos.iterativos;

public class SelectionSort {
	
	public static long selectionSort(int [] vetor){
		long numeroComparacoes = 0;
		
		for(int i = 0; i < vetor.length; i++) {
			int min = i;
			for(int j = i+1; j < vetor.length; j++) { // BUSCA O MENOR ELEMENTO ATUAL
				if(vetor[j] < vetor[min]) {
					min = j;
				}
				numeroComparacoes++; // POIS SEMPRE EXECULTAR� UMA COMPARA��O AQUI
			}
			
			if(i != min){// COLOCA O MENOR ELEMENTO DO VETOR NA POSI��O CORRETA
				int aux = vetor[i];
				vetor[i] = vetor[min];
				vetor[min] = aux;
			}
			
		}
		
		return numeroComparacoes;
	}
	
}
