package algoritmos.iterativos;

public class BubbleSort {
	
	public static long bubbleSort(int [] vetor) {
		long numeroComparacoes = 0;
		
		for(int i = vetor.length - 1; i >= 0; i--) {
			
			for(int j = 0; j < i; j++) {
				if(vetor[j] > vetor[j+1]){
					int aux = vetor[j];
					vetor[j] = vetor[j+1];
					vetor[j+1] = aux;
				}
				numeroComparacoes++; // POIS SEMPRE EXECULTARÁ UMA COMPARAÇÃO AQUI
			}
			
		}
		
		return numeroComparacoes;
	}
	
}
