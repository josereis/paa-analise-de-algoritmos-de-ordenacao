package algoritmos.iterativos;

public class InsertionSort {
	
	public static long insertionSort(int [] vetor){
		long numeroComparacoes = 0;
		
		for(int i = 0; i < vetor.length; i++){
			int j = i, chave = vetor[i];
			
			// VERIFICA E BUSCA O LOCAL DE INSER��O DO VALOR ATUAL
			while((j > 0) && (chave < vetor[j-1])){
				vetor[j] = vetor[j-1];
				j--; numeroComparacoes++; // POIS SEMPRE EXECULTAR� UMA COMPARA��O AQUI
			}
			vetor[j] = chave;
		}
		
		return numeroComparacoes;
	}
	
}
