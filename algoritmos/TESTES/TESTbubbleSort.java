package algoritmos.TESTES;

import static org.junit.Assert.*;

import org.junit.Test;
import geradorVetor.*;
import algoritmos.iterativos.*;

public class TESTbubbleSort {

	@Test
	public void test() {
		int [] vetor = GerarVetor.vetorDecrescente(100); int i = 0; boolean status = true;
		
		BubbleSort.bubbleSort(vetor);
		
		// VERIFICANDO SE O VETOR FOI ORDENADO CORRETAMENTE
		while(status && (i < vetor.length-1)){
			if(!(vetor[i] <= vetor[i+1])){
				status = false;
			}
			i++;
		}
		assertTrue(status);
	}

}
