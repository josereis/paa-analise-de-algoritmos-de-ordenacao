package algoritmos.TESTES;

import static org.junit.Assert.*;

import org.junit.Test;

import algoritmos.iterativos.InsertionSort;
import geradorVetor.*;

public class TESTinsertionSort {

	@Test
	public void testInsertionSort() {
		int [] vetor = GerarVetor.vetorDecrescente(100); int i = 0; boolean status = true;
		
		// PASSANDO VETOR A SER ORDENADO PARA O ALGORITMO
		InsertionSort.insertionSort(vetor);
		
		// VERIFICANDO SE O VETOR FOI ORDENADO CORRETAMENTE
		while(status && (i < vetor.length-1)){
			if(!(vetor[i] <= vetor[i+1])){
				status = false;
			}
			i++;
		}
		assertTrue(status);
	}

}
