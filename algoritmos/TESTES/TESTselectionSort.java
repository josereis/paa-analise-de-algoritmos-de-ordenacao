package algoritmos.TESTES;

import static org.junit.Assert.*;

import org.junit.Test;

import algoritmos.iterativos.SelectionSort;
import geradorVetor.GerarVetor;

public class TESTselectionSort {

	@Test
	public void test() {
		int [] vetor = GerarVetor.vetorDecrescente(100); int i = 0; boolean status = true;
		
		SelectionSort.selectionSort(vetor);
		
		// VERIFICANDO SE O VETOR FOI ORDENADO CORRETAMENTE
		while(status && (i < vetor.length-1)){
			if(!(vetor[i] <= vetor[i+1])){
				status = false;
			}
			i++;
		}
		assertTrue(status);
	}

}
