package execucao;

import algoritmos.iterativos.*;
import algoritmos.recursivos.*;
import java.util.GregorianCalendar;

public class TimeSort {
	private long numeroComparacoes;
	
	public long getNumeroComparacoes(){
		return numeroComparacoes;
	}
	
	// ALGORITMOS ITERATIVOS
	public long insertionSort(int [] vetor){			
		GregorianCalendar lastTime = new GregorianCalendar();
		numeroComparacoes = InsertionSort.insertionSort(vetor);
		GregorianCalendar fistTime = new GregorianCalendar();
			
		return fistTime.getTimeInMillis() - lastTime.getTimeInMillis();
	}
	
	public long selectionSort(int [] vetor){
		GregorianCalendar lastTime = new GregorianCalendar();
		numeroComparacoes = SelectionSort.selectionSort(vetor);
		GregorianCalendar fistTime = new GregorianCalendar();
		
		return fistTime.getTimeInMillis() - lastTime.getTimeInMillis();
	}
	
	public long bubbleSort(int [] vetor){	
		GregorianCalendar lastTime = new GregorianCalendar();
		numeroComparacoes = BubbleSort.bubbleSort(vetor);
		GregorianCalendar fistTime = new GregorianCalendar();
		
		return fistTime.getTimeInMillis() - lastTime.getTimeInMillis();
	}
	
	// ALGORITMOS COM RECURS�O
	public long mergeSort(int [] vetor){
		GregorianCalendar lastTime = new GregorianCalendar();
		numeroComparacoes = MergeSort.mergeSort(vetor);
		GregorianCalendar fistTime = new GregorianCalendar();
		
		return fistTime.getTimeInMillis() - lastTime.getTimeInMillis();
	}
	
	public double heapSort(int [] vetor){
		GregorianCalendar lastTime = new GregorianCalendar();
		numeroComparacoes = HeapSort.heapSort(vetor);
		GregorianCalendar fistTime = new GregorianCalendar();
			
		return fistTime.getTimeInMillis() - lastTime.getTimeInMillis();
	}
	
	public double quickSort(int [] vetor){
		GregorianCalendar lastTime = new GregorianCalendar();
		QuickSort.quickSort(vetor);
		GregorianCalendar fistTime = new GregorianCalendar();
			
		return fistTime.getTimeInMillis() - lastTime.getTimeInMillis();
	}
	
}
