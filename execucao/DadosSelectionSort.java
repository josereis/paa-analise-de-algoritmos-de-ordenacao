package execucao;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;

import geradorVetor.GerarVetor;

public class DadosSelectionSort {
	private static final int CRESCENTE = 0, DECRESCENTE = 1, ALEATORIO = 2, QMAX = 3;
	private static final int [] vetorTamanhos = {100, 500, 1000, 5000, 30000, 80000, 100000, 150000, 200000};
	
	// ESTRUTURA QUE CONTERAM OS DADOS USADOS PARA CALCULO DAS MEDIAS PARA A EFETUAÇÃO DAS COMPARAÇÕES
	private static long [][] tempos = new long[vetorTamanhos.length][QMAX], comparacoes = new long[vetorTamanhos.length][QMAX];
	
	public static void salvarDados(String nameArquivo) throws IOException{
		FileWriter arquivo = new FileWriter(nameArquivo);
		PrintWriter gravarArquivo = new PrintWriter(arquivo);
		
		gravarArquivo.println("SELECTION SORT -> DADOS DE TEMPOS PARA VETOR");
		for(int i = 0; i < vetorTamanhos.length; i++){
			gravarArquivo.println("TAMANHO " + vetorTamanhos[i] + "	: " + Arrays.toString(tempos[i]));
		}
		
		gravarArquivo.println("\n\nSELECTION SORT -> DADOS DE COMPARAÇÕES PARA VETOR");
		for(int i = 0; i < vetorTamanhos.length; i++){
			gravarArquivo.println("TAMANHO " + vetorTamanhos[i] + "	: " + Arrays.toString(comparacoes[i]));
		}
		
		arquivo.close();
	}
	
	private static int [] criarVetor(int tipo, int tamanho){
		// RETORNARÁ UM VETOR DEPENDENO DO TIPO PASSADO E TAMANHO
		if(tipo == CRESCENTE) {
			return GerarVetor.vetorCrescente(tamanho);
		} else if(tipo == DECRESCENTE) {
			return GerarVetor.vetorDecrescente(tamanho);
		} else if(tipo == ALEATORIO) {
			return GerarVetor.vetorAleatorio(tamanho);
		} else
			return null;
	}
	
	private static void timeSort(int tipo) {
		int [] vetor; TimeSort timeSort = new TimeSort();
		
		// CALCULAR OS TEMPOS PARA CADA UM DOS TAMANHOS PASSADOS
		for(int i = 0; i < vetorTamanhos.length; i++){
			// CALCULA O TEMPO 'QMAX' VEZES PARA SE CALCULAR UMA MEDIA DOS TEMPOS
			for(int j = 0; j < QMAX; j++){
				vetor = criarVetor(tipo, vetorTamanhos[i]);
				
				tempos[i][j] = timeSort.selectionSort(vetor);
				comparacoes[i][j] = timeSort.getNumeroComparacoes();
			}
		}
		
	}
	
	public static void main(String[] args) {
		System.out.println("Iniciado o sistema para o Selection Sort...\n\n");
		
		String nameArquivo;
		for(int i = 0; i < QMAX; i++) {
			if(i == CRESCENTE){
				timeSort(CRESCENTE);
				nameArquivo = "SELECTIONSORT(CRESCENTE).txt";
			} else if(i == DECRESCENTE) {
				timeSort(DECRESCENTE);
				nameArquivo = "SELECTIONSORT(DECRESCENTE).txt";
			}else{
				timeSort(ALEATORIO);
				nameArquivo = "SELECTIONSORT(ALEATORIO).txt";
			}
			
			try{
				salvarDados(nameArquivo);
			} catch (Exception e) {
				System.out.println("ERRO AO SALVAR DADOS EM ARQUIVO: " + e.getMessage());
			}
			
		}
		
		System.out.println("\n\n... Finalizado o sistema para o Selection Sort");
	}
	
}
